<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('data.form');
    }

    public function login(Request $request){
        $first = $request->firstname;
        $last = $request->lastname;
        return view('data.index', compact('first','last'));
    }
}
