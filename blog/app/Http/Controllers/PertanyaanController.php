<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
            $request->validate([
                'id' => 'required|unique:pertanyaan',
                'judul' => 'required',
                'isi' => 'required'
            ]);

            $query = DB::table('pertanyaan')->insert([
                "id" => $request["id"],
                "judul" => $request["judul"],
                "isi" => $request["isi"],
                "tanggal_dibuat" => $request["tanggaldibuat"],
                "tanggal_diperbaharui" => $request["tanggaldiperbaharui"],
                "profil_id" => $request["profilid"]
            ]);

            return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan');
    }

    public function index(){
        $pertanyaans = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaans'));
    }

    public function show($id) {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id,Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);
        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul'=>$request['judul'],
                        'isi'=>$request['isi']
                    ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil Update Pertanyaan');
    }

    public function destroy($id) {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Berhasil Menghapus Pertanyaan');
    }
}
